#ifndef ETH_SMB_H
#define ETH_SMB_H

/*
 * Driver Interface with the IP CORE (ETH_SMB)
 */
#include "Fpgaipm.h"


/******************************************************************************
 *                            CONSTANTS
 ******************************************************************************/
#define ETH_SMB_CORE_ID				(1)

/**** OPCODES ****/
#define OPC_CSR_READ				((uint8_t)(0x01))
#define OPC_CSR_WRITE				((uint8_t)(0x02))
#define OPC_TX_LAN					((uint8_t)(0x09))
#define OPC_START_TIMER				((uint8_t)(0x03))
#define OPC_RESET_TIMER				((uint8_t)(0x06))
#define OPC_STOP_TIMER				((uint8_t)(0x05))
#define OPC_CHANGE_TIMER_PERIOD		((uint8_t)(0x04))

/**** DATA BUFFER ADDRESSES ****/
#define ETH_SMB_ADDREG				(1)
#define ETH_SMB_LOCKREG				(63)

#define ETH_SMB_DATAH				(2)
#define ETH_SMB_DATAL				(3)

#define ETH_SMB_TIMER_DATAH 		(4)
#define ETH_SMB_TIMER_DATAL 		(5)

#define ETH_SMB_TX_FRAME_LENGTH_REG (1)

#define CPU_UNLOCK_WORD				(0xFFFF)
#define CPU_LOCK_WORD				(0x0000)


/******************************************************************************
 *                            INTERFACE FUNCTIONS
 *****************************************************************************/
void ETH_SMB_PIORead(uint16_t lan9211_reg, uint32_t *data);
void ETH_SMB_PIOWrite(uint16_t lan9211_reg, uint32_t data);
void ETH_SMB_TX(uint8_t frame[], uint16_t array_length, uint32_t txA, uint32_t txB);

#endif // ETH_SMB_H
