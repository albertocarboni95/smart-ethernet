# Smart Ethernet
Project for the System Design Project course at Politecnico di Torino.

A special system on chip called SEcubeTM by Blu5 Group, including an ARM Cortex-M4 processor, an FPGA and a Smart Card, has been equipped to support Ethernet. The project included a custom design on FPGA (Lattice MachXO2) dedicated to connecting the CPU to a LAN controller, writing the drivers and drafting the required documentation.
