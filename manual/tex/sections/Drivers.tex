%\usepackage{listings}
\section{API}
In this chapter the communication protocol provided by the Application Programming Interface is explained. This is divided into high-level and low-level API; the former is used to configure the system and to start a transmission, the latter to take care of the communication with the IP-Manager. All the macros for each addressable register are defined in the lan9211.h file.

\subsection{Low-level API}
The low-level API consists in two functions which allow the upper layers to write and to read LAN9211's internal registers. Each register is 32 bits wide and it is addressable via a 16 bits value between 50h and FCh\footnote{Please refer to the LAN9211 Data Sheet, Chapter 5, Table 5-1: DIRECT ADDRESS REGISTER MAP}.

\begin{lstlisting}
void ETH_SMB_PIOWrite(uint16_t lan9211_reg, uint32_t data)
\end{lstlisting}
The ETH\_SMB\_PIOWrite function is used to write a word of 32 bits into a LAN9211 register. The following parameters are required:
\begin{itemize}
\item \lstinline{lan9211_reg}: the ADDRESS of the required register;
\item \lstinline{data}: The VALUE to be written.
\end{itemize}

\begin{lstlisting}
void ETH_SMB_PIORead(uint16_t lan9211_reg, uint32_t *data)
\end{lstlisting}
This function is used to read 32 bits from a LAN9211 register. The following parameters are required:
\begin{itemize}
\item \lstinline{lan9211_reg}: the ADDRESS of the required register;
\item \lstinline{data}: the POINTER to the memory location where the data will be stored.
\end{itemize}

\subsection{High-level API}
They provide the following services to the user:
\begin{itemize}
\item Initialize the LAN9211 controller;
\item Send an Ethernet frame;
\item Receive an Ethernet frame;
\item Read/Write CSR registers;
\item Read/Write PHY registers;
\item Read/Write MAC registers;
\item Dump all LAN9211 registers.
\end{itemize}

\subsubsection{Basic Functions}
With these functions it is possible to use the LAN9211 controller with a basic configuration chosen to meet the architectural constraints.
\begin{lstlisting}
uint8_t lan9211_init()
\end{lstlisting}
This function initializes the LAN9211 controller with a default configuration:
\begin{itemize}
\item Perform an HW soft Reset;
\item Assure that the internal PHY is running;
\item Set Automatic Flow Control;
\item Turn on GPIO leds;
\item Disable and clear interrupts;
\item Enable flow control and pause frame time;
\item Set default MAC Address;
\item Initialize TX parameters;
\item Initialize RX parameters;
\item Enable TX and RX;
%\item Enable TX and RX interrupts;
\item Initialize PHY parameters;
\item Start auto negotiation;
\end{itemize}

\begin{lstlisting}
int lan9211_sendFrame(const uint8_t *frame, uint16_t length);
\end{lstlisting}
This function is used to send an Ethernet frame. The following parameters are required:
\begin{itemize}
\item \lstinline{frame}: the FRAME to send;
\item \lstinline{length}: the LENGTH of the frame;
\end{itemize}
It returns an integer value:
\begin{itemize}
\item Negative value: an error occurred in the transmission of the frame;
\item 0: no frame sent;
\item Positive Value: the length of the transmitted frame.
\end{itemize}
Steps:
\begin{itemize}
\item Add padding (if necessary);
\item Generate TX command A and TX command B;
\item Send the two commands followed by the frame;
\item Check for errors.
\end{itemize}

\begin{lstlisting}
int lan9211_receiveFrame(uint8_t *buffer);
\end{lstlisting}
This function is called to receive a frame. The following parameters are required:
\begin{itemize}
\item \lstinline{buffer}: the buffer where the received frame will be stored;
\end{itemize}
It returns an integer value:
\begin{itemize}
\item Negative value: an error occurred in the reception of the frame;
\item 0: no frames collected;
\item Positive Value: the length of the received frame.
\end{itemize}
Steps:
\begin{itemize}
\item Read \lstinline{RX_FIFO_INF} register to check if there is used space (occupied by the incoming frame);
\item Check the status port for errors;
\item Read the whole frame and store it.
\end{itemize}

\begin{lstlisting}
void CSRregsDump()
\end{lstlisting}
This functions reads all the main PIO registers and store their value on a struct variable called 'regs'.

\subsubsection{Advanced Functions}
These functions can be used to change the system configuration. The use of this function could compromise the functioning of the system, hence use them only after understanding the hardware architecture.

\begin{lstlisting}
void SetMAC_Reg(uint8_t idx, uint32_t data)
\end{lstlisting}
This function is used to write an internal register of the MAC. The following parameters are required:
\begin{itemize}
\item \lstinline{lan9211_reg}: the ADDRESS of the required MAC register;
\item \lstinline{data}: The VALUE to be written.
\end{itemize}

\begin{lstlisting}
void GetMAC_Reg(uint8_t idx, uint32_t *data)
\end{lstlisting}
This function is used to read an internal register of the MAC. The following parameters are required:
\begin{itemize}
\item \lstinline{idx}: the ADDRESS of the required MAC register;
\item \lstinline{data}: The POINTER to the memory location where the data will be stored.
\end{itemize}

\begin{lstlisting}
void SetPHY_Reg(uint8_t idx, uint32_t data)
\end{lstlisting}
This function is used to write to a PHY register of the LAN controller. The following parameters are required:
\begin{itemize}
\item \lstinline{idx}: the ADDRESS of the required PHY register;
\item \lstinline{data}: The VALUE to be written.
\end{itemize}

\begin{lstlisting}
void GetPHY_Reg(uint8_t idx, uint32_t *data)
\end{lstlisting}
This function is used to read a PHY register of the LAN controller. The following parameters are required:
\begin{itemize}
\item \lstinline{idx}: the ID of the required PHY register;
\item \lstinline{data}: The POINTER to the memory location where the data will be stored.
\end{itemize}

\begin{lstlisting}
void lan9211_CSR_write(uint16_t lan9211_reg, uint32_t data)
\end{lstlisting}
This function is used to write a value into a CSR (Control and Status Register) of the LAN9211. The following parameters are required:
\begin{itemize}
\item \lstinline{lan9211_reg}: the ADDRESS of the required PIO register;
\item \lstinline{data}: The VALUE to be written.
\end{itemize}

\begin{lstlisting}
void lan9211_CSR_read(uint16_t lan9211_reg, uint32_t *data)
\end{lstlisting}
This function is used to read the value of a CSR (Control and Status register) of the LAN9211. The following parameters are required:
\begin{itemize}
\item \lstinline{lan9211_reg}: the ADDRESS of the required PIO register;
\item \lstinline{data}: The POINTER to the memory location where the data will be stored.
\end{itemize}

\newpage
